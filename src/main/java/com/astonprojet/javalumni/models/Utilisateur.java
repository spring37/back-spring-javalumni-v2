package com.astonprojet.javalumni.models;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "utilisateur")
@NoArgsConstructor
@AllArgsConstructor
public class Utilisateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idutilisateur;
    @Column(unique = true)
	private Integer numPE;
    private String formation;
    @Column(unique = true)
	private Integer numRCS;
    @Column()
	private String nom;
	@Column()
	private String prenom;
	@Column()
	private String promotion;
	@Column()
	@Email
	private String email;
	@Column()
	private String password;
	@Column()
	private String nomEntreprise;
	@Column()
	private String ville;
	@Column()
	private String presentation;
	@Column
	private Date dateDebut;
	@Column
	private Date dateFin;
	@Column()
	private String linkedin;
	@Column()
	private String siteWeb;
	@Column()
	private Long projetSoutenu;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "projet_idprojet", nullable = true)
    private Projet projet;
	    
}
