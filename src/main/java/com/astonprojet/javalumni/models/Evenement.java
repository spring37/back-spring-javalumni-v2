package com.astonprojet.javalumni.models;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "evenement")
@NoArgsConstructor
@AllArgsConstructor
public class Evenement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idevenement;
	@NotNull
	private String nom;
	@Column()
	private String description;
	@Column()
	private String localisation;
	@Column()
	private Date dateEvenment;
	@Column()
	private Date datePublication;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "utilisateur_idutilisateur", nullable = true)
    private Utilisateur utilisateur;
}