package com.astonprojet.javalumni.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data

@Table(name = "projet")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Projet {
		
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idprojet;
	@NotNull
	private String titre;
	@NotNull
	@Column()
	private String description;
	@NotNull
	@Column()
	private String fonctionnalites;
	@NotNull
	@Column()
	private String technologies;
	@NotNull
	@Column()
	private String groupe;
	@NotNull
	@Column(columnDefinition = "varchar(1000) default 'enCours'")
	private String etat;

}
