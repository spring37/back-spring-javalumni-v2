package com.astonprojet.javalumni.models;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "reponsefaq")
@NoArgsConstructor
@AllArgsConstructor
public class ReponseFaq {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idreponsefaq;
	@NotNull
	private String reponse;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "faq_idfaq", nullable = true)
    private Faq faq;
	
}