package com.astonprojet.javalumni.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "reponse")
@NoArgsConstructor
@AllArgsConstructor
public class Reponse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idreponse;
	@NotNull
	private String reponse;
	@Column()
	@NotNull
	private byte bonneReponse;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "question_idquestion", nullable = true)
    private Question question;
}