package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.astonprojet.javalumni.models.Question;
import com.astonprojet.javalumni.repositories.QuestionRepository;
@Service
public class QuestionService {
	@Autowired
	private QuestionRepository repository;
	public Question save(Question entity) {
		return this.repository.save(entity);
	}
	public Question saveOrUpdate(Question entity) {
		return this.repository.save(entity);
	}
	public List<Question> findAll() {
		return this.repository.findAll();
	}
	public Optional<Question> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}