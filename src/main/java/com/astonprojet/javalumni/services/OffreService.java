package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astonprojet.javalumni.models.Offre;
import com.astonprojet.javalumni.repositories.OffreRepository;
@Service
public class OffreService {
	@Autowired
	private OffreRepository repository;
	public Offre save(Offre entity) {
		return this.repository.save(entity);
	}
	public Offre saveOrUpdate(Offre entity) {
		return this.repository.save(entity);
	}
	public List<Offre> findAll() {
		return this.repository.findAll();
	}
	
	public Offre findFirstByOrderByIdoffreDesc() {
		return this.repository.findFirstByOrderByIdoffreDesc();
	}
	
	public Optional<Offre> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}