package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astonprojet.javalumni.models.Evenement;
import com.astonprojet.javalumni.repositories.EvenementRepository;
@Service
public class EvenementService {
	@Autowired
	private EvenementRepository repository;
	public Evenement save(Evenement entity) {
		return this.repository.save(entity);
	}
	public Evenement saveOrUpdate(Evenement entity) {
		return this.repository.save(entity);
	}
	public List<Evenement> findAll() {
		return this.repository.findAll();
	}
	
	public Evenement findFirstByOrderByIdevenementDesc() {
		return this.repository.findFirstByOrderByIdevenementDesc();
	}
	
	public Optional<Evenement> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}