package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.astonprojet.javalumni.models.ReponseFaq;
import com.astonprojet.javalumni.repositories.ReponseFaqRepository;
@Service
public class ReponseFaqService {
	@Autowired
	private ReponseFaqRepository repository;
	public ReponseFaq save(ReponseFaq entity) {
		return this.repository.save(entity);
	}
	public ReponseFaq saveOrUpdate(ReponseFaq entity) {
		return this.repository.save(entity);
	}
	public List<ReponseFaq> findAll() {
		return this.repository.findAll();
	}
	public Optional<ReponseFaq> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}