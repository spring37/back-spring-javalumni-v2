package com.astonprojet.javalumni.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astonprojet.javalumni.models.Projet;
import com.astonprojet.javalumni.models.Utilisateur;
import com.astonprojet.javalumni.repositories.ProjetCustomRepository;
import com.astonprojet.javalumni.repositories.ProjetRepository;

import lombok.Data;

@Service
public class ProjetService {

	@Autowired
	private ProjetRepository repository;
	@Autowired
	private ProjetCustomRepository customrepository;
	
	public Projet save(Projet entity) {
		entity.setEtat("enCours");
		return this.repository.save(entity);
	}
	
	public Projet saveOrUpdate(Projet entity) {
		return this.repository.save(entity);
	}
		
	public List<Utilisateur> membresProjet(Long idprojet) {
		return this.customrepository.membresProjet(idprojet);
	}
	
	public List<Projet> findAll() {
		return this.repository.findAll();
	}
	
	public long count() {
		return this.repository.count();
	}
	
	public long countByEtat(String etat) {
		return this.repository.countByEtat(etat);
	}

	public Optional<Projet> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Projet> findByEtat(String projet) {
		return this.repository.findByEtat(projet);
	}
	
	public List<String> test() {
		return this.repository.test();
	}
	

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}
	
}
