package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.astonprojet.javalumni.models.Reponse;
import com.astonprojet.javalumni.repositories.ReponseRepository;
@Service
public class ReponseService {
	@Autowired
	private ReponseRepository repository;
	public Reponse save(Reponse entity) {
		return this.repository.save(entity);
	}
	public Reponse saveOrUpdate(Reponse entity) {
		return this.repository.save(entity);
	}
	public List<Reponse> findAll() {
		return this.repository.findAll();
	}
	public Optional<Reponse> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}