package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.astonprojet.javalumni.models.Commentaire;
import com.astonprojet.javalumni.repositories.CommentaireRepository;

import lombok.Data;

@Data
@Service
public class CommentaireService {
	@Autowired
	private CommentaireRepository repository;
	public Commentaire save(Long idprojet, Commentaire entity) {
		entity.setIdprojet(idprojet);
		return this.repository.save(entity);
	}
	public Commentaire saveOrUpdate(Commentaire entity) {
		return this.repository.save(entity);
	}
	public List<Commentaire> findAll() {
		return this.repository.findAll();
	}
	
	public List<Commentaire> findAllCommentaires() {
		return this.repository.findAllCommentaires();
	}
	
	public Optional<Commentaire> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}