package com.astonprojet.javalumni.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astonprojet.javalumni.models.Git;
import com.astonprojet.javalumni.repositories.GitRepository;

@Service
public class GitService {

	@Autowired
	private GitRepository repository;
	
	public Git save(Long idprojet, Git entity) {
		entity.setIdprojet(idprojet);
		return this.repository.save(entity);
	}
	
	public Git saveOrUpdate(Git entity) {
		return this.repository.save(entity);
	}
		
	public List<Git> findAll() {
		return this.repository.findAll();
	}


	public Optional<Git> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Git> findAllGits() {
		return this.repository.findAllGits();
	}
	

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}
	
}
