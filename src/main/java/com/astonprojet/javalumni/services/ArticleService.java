package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.astonprojet.javalumni.models.Article;
import com.astonprojet.javalumni.repositories.ArticleRepository;
@Service
public class ArticleService {
	@Autowired
	private ArticleRepository repository;
	public Article save(Article entity) {
		return this.repository.save(entity);
	}
	public Article saveOrUpdate(Article entity) {
		return this.repository.save(entity);
	}
	public List<Article> findAll() {
		return this.repository.findAll();
	}
	
	public Article findFirstByOrderByIdarticleDesc() {
		return this.repository.findFirstByOrderByIdarticleDesc();
	}
		
	public Optional<Article> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}