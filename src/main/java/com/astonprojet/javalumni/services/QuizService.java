package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astonprojet.javalumni.models.Quiz;
import com.astonprojet.javalumni.repositories.QuizRepository;
@Service
public class QuizService {
	@Autowired
	private QuizRepository repository;
	public Quiz save(Quiz entity) {
		return this.repository.save(entity);
	}
	public Quiz saveOrUpdate(Quiz entity) {
		return this.repository.save(entity);
	}
	public List<Quiz> findAll() {
		return this.repository.findAll();
	}
	
	public Quiz findFirstByOrderByIdquizDesc() {
		return this.repository.findFirstByOrderByIdquizDesc();
	}
	
	public Optional<Quiz> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}