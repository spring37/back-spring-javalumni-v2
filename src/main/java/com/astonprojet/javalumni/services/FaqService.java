package com.astonprojet.javalumni.services;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.astonprojet.javalumni.models.Faq;
import com.astonprojet.javalumni.repositories.FaqRepository;
@Service
public class FaqService {
	@Autowired
	private FaqRepository repository;
	public Faq save(Faq entity) {
		return this.repository.save(entity);
	}
	public Faq saveOrUpdate(Faq entity) {
		return this.repository.save(entity);
	}
	public List<Faq> findAll() {
		return this.repository.findAll();
	}
	
	public Faq findFirstByOrderByIdfaqDesc() {
		return this.repository.findFirstByOrderByIdfaqDesc();
	}
	
	public Optional<Faq> findById(Long id) {
		return this.repository.findById(id);
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}