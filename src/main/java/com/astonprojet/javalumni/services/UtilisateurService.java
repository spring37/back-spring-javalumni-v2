package com.astonprojet.javalumni.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.astonprojet.javalumni.models.Utilisateur;
import com.astonprojet.javalumni.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository repository;
	
	public Utilisateur save(Utilisateur entity) {
		return this.repository.save(entity);
	}
	
	public Utilisateur saveOrUpdate(Utilisateur entity) {
		return this.repository.save(entity);
	}
	
	// Pour faire la v�rif
	public Utilisateur login(String email, String password) {
		return this.repository.login(email, password);
	}
	
	public List<Utilisateur> findAll() {
		return this.repository.findAll();
	}
	

	public List<Utilisateur> findAllProjetSoutenu(Long projetSoutenu) {
		return this.repository.findAllProjetSoutenu(projetSoutenu);
	}
	
	public List<Utilisateur> findAllNumPE() {
		return this.repository.findAllNumPE();
	}
	
	public List<Utilisateur> findAllNumRCS() {
		return this.repository.findAllNumRCS();
	}
	
	public List<Utilisateur> findByProjet_idprojet(Long projet_idprojet) {
		return this.repository.findByProjet_idprojet(projet_idprojet);
	}
	
	public List<Utilisateur> findByFormation(String formation) {
		return this.repository.findByFormation(formation);
	}
	
	public Optional<Utilisateur> findById(Long id) {
		return this.repository.findById(id);
	}
	

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

}
