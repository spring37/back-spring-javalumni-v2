package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astonprojet.javalumni.models.Evenement;
import com.astonprojet.javalumni.services.EvenementService;
@CrossOrigin
@RestController
@RequestMapping("evenements")
public class EvenementController {
	@Autowired
	private EvenementService service;
	@GetMapping("")
	public List<Evenement> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public Evenement save(@RequestBody Evenement entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public Evenement saveOrUpdate(@RequestBody Evenement entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Evenement> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("dernierEvenement")
	public Evenement findFirstByOrderByIdevenementDesc(){
		return this.service.findFirstByOrderByIdevenementDesc();
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}