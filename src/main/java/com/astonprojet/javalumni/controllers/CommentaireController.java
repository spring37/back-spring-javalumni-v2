package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.astonprojet.javalumni.models.Commentaire;
import com.astonprojet.javalumni.services.CommentaireService;
@RestController
@RequestMapping("commentaires")
public class CommentaireController {
	@Autowired
	private CommentaireService service;
	@GetMapping("")
	public List<Commentaire> findAll(){
		return this.service.findAll();
	}

	@PutMapping("")
	public Commentaire saveOrUpdate(@RequestBody Commentaire entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Commentaire> findById(@PathVariable Long id){
		return this.service.findById(id);
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}