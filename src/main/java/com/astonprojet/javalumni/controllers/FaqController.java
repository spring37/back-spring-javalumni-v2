package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.astonprojet.javalumni.models.Faq;
import com.astonprojet.javalumni.services.FaqService;
@RestController
@RequestMapping("faqs")
public class FaqController {
	@Autowired
	private FaqService service;
	@GetMapping("")
	public List<Faq> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public Faq save(@RequestBody Faq entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public Faq saveOrUpdate(@RequestBody Faq entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Faq> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("dernierFaq")
	public Faq findFirstByOrderByIdfaqDesc(){
		return this.service.findFirstByOrderByIdfaqDesc();
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}