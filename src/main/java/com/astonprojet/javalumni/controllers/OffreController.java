package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astonprojet.javalumni.models.Offre;
import com.astonprojet.javalumni.services.OffreService;

@CrossOrigin
@RestController
@RequestMapping("offres")
public class OffreController {
	@Autowired
	private OffreService service;
	@GetMapping("")
	public List<Offre> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public Offre save(@RequestBody Offre entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public Offre saveOrUpdate(@RequestBody Offre entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Offre> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("derniereOffre")
	public Offre findFirstByOrderByIdoffreDesc(){
		return this.service.findFirstByOrderByIdoffreDesc();
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}