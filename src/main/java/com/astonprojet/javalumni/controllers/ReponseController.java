package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.astonprojet.javalumni.models.Reponse;
import com.astonprojet.javalumni.services.ReponseService;
@RestController
@RequestMapping("reponses")
public class ReponseController {
	@Autowired
	private ReponseService service;
	@GetMapping("")
	public List<Reponse> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public Reponse save(@RequestBody Reponse entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public Reponse saveOrUpdate(@RequestBody Reponse entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Reponse> findById(@PathVariable Long id){
		return this.service.findById(id);
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}