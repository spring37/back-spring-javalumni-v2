package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.astonprojet.javalumni.models.Article;
import com.astonprojet.javalumni.services.ArticleService;

@CrossOrigin
@RestController
@RequestMapping("articles")
public class ArticleController {
	@Autowired
	private ArticleService service;
	@GetMapping("")
	public List<Article> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public Article save(@RequestBody Article entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public Article saveOrUpdate(@RequestBody Article entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Article> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("dernierArticle")
	public Article findFirstByOrderByIdarticleDesc(){
		return this.service.findFirstByOrderByIdarticleDesc();
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}