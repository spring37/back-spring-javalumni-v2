package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.astonprojet.javalumni.models.ReponseFaq;
import com.astonprojet.javalumni.services.ReponseFaqService;
@RestController
@RequestMapping("reponsesfaqs")
public class ReponseFaqController {
	@Autowired
	private ReponseFaqService service;
	@GetMapping("")
	public List<ReponseFaq> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public ReponseFaq save(@RequestBody ReponseFaq entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public ReponseFaq saveOrUpdate(@RequestBody ReponseFaq entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<ReponseFaq> findById(@PathVariable Long id){
		return this.service.findById(id);
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}