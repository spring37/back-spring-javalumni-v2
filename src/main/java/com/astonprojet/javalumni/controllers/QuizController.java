package com.astonprojet.javalumni.controllers;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astonprojet.javalumni.models.Quiz;
import com.astonprojet.javalumni.services.QuizService;
@RestController
@RequestMapping("quizs")
public class QuizController {
	@Autowired
	private QuizService service;
	@GetMapping("")
	public List<Quiz> findAll(){
		return this.service.findAll();
	}
	@PostMapping("")
	public Quiz save(@RequestBody Quiz entity){
		return this.service.save(entity);
	}	
	@PutMapping("")
	public Quiz saveOrUpdate(@RequestBody Quiz entity){
		return this.service.saveOrUpdate(entity);
	}
	@GetMapping("{id}")
	public Optional<Quiz> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("dernierQuiz")
	public Quiz findFirstByOrderByIdquizDesc(){
		return this.service.findFirstByOrderByIdquizDesc();
	}

	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
}