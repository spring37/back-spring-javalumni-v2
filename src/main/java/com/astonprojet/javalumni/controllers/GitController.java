package com.astonprojet.javalumni.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astonprojet.javalumni.models.Git;
import com.astonprojet.javalumni.services.GitService;

@RestController
@RequestMapping("gits")


public class GitController {
	
	@Autowired
	private GitService service;
	
	@GetMapping("")
	public List<Git> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("")
	public Git save(@PathVariable Long id, @RequestBody Git entity){
		return this.service.save(id, entity);
	}	
	
	@PutMapping("")
	public Git saveOrUpdate(@RequestBody Git entity){
		return this.service.saveOrUpdate(entity);
	}
	

	@GetMapping("{id}")
	public Optional<Git> findById(@PathVariable Long id){
		return this.service.findById(id);
	}

	
	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
		
}
