package com.astonprojet.javalumni.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astonprojet.javalumni.models.Utilisateur;
import com.astonprojet.javalumni.services.UtilisateurService;

@RestController
@RequestMapping("utilisateurs")

@CrossOrigin
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService service;
	
	@GetMapping("")
	public List<Utilisateur> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("")
	public Utilisateur save(@RequestBody Utilisateur entity){
		return this.service.save(entity);
	}
	
	// Pour faire de la v�rif
	@PostMapping("inscription/verifEmail/{email}/{password}")
	public Utilisateur login(@PathVariable String email, @PathVariable String password){
		System.out.println(email);
		return this.service.login(email, password);
	}
	
	@PutMapping("")
	public Utilisateur saveOrUpdate(@RequestBody Utilisateur entity){
		return this.service.saveOrUpdate(entity);
	}
	

	@GetMapping("{id}")
	public Optional<Utilisateur> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("projets/{projet_idprojet}")
	public List<Utilisateur> findByProjet_idprojet(@PathVariable Long projet_idprojet){
		return this.service.findByProjet_idprojet(projet_idprojet);
	}
	
	@GetMapping("beneficiaires")
	public List<Utilisateur> findAllNumPE(){
		return this.service.findAllNumPE();
	}
	
	@GetMapping("entreprises")
	public List<Utilisateur> findAllNumRCS(){
		return this.service.findAllNumRCS();
	}
	
	@GetMapping("beneficiaires/{formation}")
	public List<Utilisateur> findByFormation(@PathVariable String formation){
		return this.service.findByFormation(formation);
	}

	
	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
		
}
