package com.astonprojet.javalumni.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.astonprojet.javalumni.models.Commentaire;
import com.astonprojet.javalumni.models.Git;
import com.astonprojet.javalumni.models.Projet;
import com.astonprojet.javalumni.models.Utilisateur;
import com.astonprojet.javalumni.repositories.ProjetCustomRepository;
import com.astonprojet.javalumni.services.CommentaireService;
import com.astonprojet.javalumni.services.GitService;
import com.astonprojet.javalumni.services.ProjetService;
import com.astonprojet.javalumni.services.UtilisateurService;

@CrossOrigin
@RestController
@RequestMapping("projets")


public class ProjetController {
	
	@Autowired
	private ProjetService service;
	@Autowired
	private CommentaireService commentaireService;
	@Autowired
	private GitService gitService;
	@Autowired
	private UtilisateurService utilisateurService;
	@Autowired
	private ProjetCustomRepository projetRepository;

	
	@GetMapping("")
	public List<Projet> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("etats/{etat}")
	public List<Projet> findByEtat(@PathVariable String etat){
		return this.service.findByEtat(etat);
	}
	
	@GetMapping("etats")
	public List<String> test(){
		return this.service.test();
	}
	
	@GetMapping("etats/{etat}/nb")
	public long countByEtat(@PathVariable String etat){
		return this.service.countByEtat(etat);
	}
	
	@GetMapping("etats/total/nb")
	public long count(){
		return this.service.count();
	}
	
	@PostMapping("")
	public Projet save(@RequestBody Projet entity){
		return this.service.save(entity);
	}
	
	@GetMapping("{id}/commentaires")
	public List<Commentaire> findAllCommentaires(){
		return this.commentaireService.findAllCommentaires();
	}
		
	@GetMapping("{projetSoutenu}/soutiens")
	public List<Utilisateur> findAllProjetSoutenu(@PathVariable Long projetSoutenu){
		return this.utilisateurService.findAllProjetSoutenu(projetSoutenu);
	}
	
	@PostMapping("{id}/commentaires")
	public Commentaire save(@PathVariable Long id, @RequestBody Commentaire entity){
		return this.commentaireService.save(id, entity);
	}	
	

	@GetMapping("{id}/gits")
	public List<Git> findAllGits(){
		return this.gitService.findAllGits();
	}
	

	@PostMapping("{id}/gits")
	public Git save(@PathVariable Long id, @RequestBody Git entity){
		return this.gitService.save(id, entity);
	}	
	
	@PutMapping("")
	public Projet saveOrUpdate(@RequestBody Projet entity){
		return this.service.saveOrUpdate(entity);
	}

	@GetMapping("{id}")
	public Optional<Projet> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("{idprojet}/membres")
	public List<Utilisateur> membresProjet(@PathVariable Long idprojet){
		return this.projetRepository.membresProjet(idprojet);
	}
	
	
	@DeleteMapping("{id}")
	public void deleteById(@PathVariable Long id){
		this.service.deleteById(id);
	}
		
}
