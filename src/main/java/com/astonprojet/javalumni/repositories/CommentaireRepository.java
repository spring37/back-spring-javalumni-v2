package com.astonprojet.javalumni.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.Commentaire;

@Repository
public interface CommentaireRepository extends JpaRepository<Commentaire, Long>{
    @Query("FROM Commentaire")
    List<Commentaire> findAllCommentaires();

}