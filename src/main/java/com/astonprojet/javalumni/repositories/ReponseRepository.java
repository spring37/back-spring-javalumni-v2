package com.astonprojet.javalumni.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.Reponse;

@Repository
public interface ReponseRepository extends JpaRepository<Reponse, Long>{


}