package com.astonprojet.javalumni.repositories;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.Utilisateur;

@Repository
public class ProjetCustomRepository {
	
		@Autowired
		private JdbcTemplate jdbcTemplate;
		  
		public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {  
		    this.jdbcTemplate = jdbcTemplate;  
		}  
		  
		public List<Utilisateur> membresProjet(Long idprojet){  
		    String sql="select * from utilisateur where projet_idprojet = ?1";  
		    return jdbcTemplate.queryForList(sql, Utilisateur.class, idprojet);
		}
		
}

