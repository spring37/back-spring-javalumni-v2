package com.astonprojet.javalumni.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.Git;

@Repository
public interface GitRepository extends JpaRepository<Git, Long>{
    @Query("FROM Git")
    List<Git> findAllGits();
	
}
