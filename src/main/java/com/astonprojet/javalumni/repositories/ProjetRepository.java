package com.astonprojet.javalumni.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.Projet;

@Repository
public interface ProjetRepository extends JpaRepository<Projet, Long>{
    @Query("FROM Projet WHERE etat = ?1")
    List<Projet> findByEtat(String projet);
    
	long countByEtat(String etat);
	
	@Query(value = "SELECT DISTINCT etat FROM Projet")
	List<String> test();

}