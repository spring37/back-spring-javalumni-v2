package com.astonprojet.javalumni.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.Utilisateur;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long>{
    @Query("FROM Utilisateur WHERE projet_idprojet = ?1")
    List<Utilisateur> findByProjet_idprojet(Long projet_idprojet);
    
    @Query("FROM Utilisateur WHERE numPE IS NOT NULL ORDER BY idutilisateur DESC")
    List<Utilisateur> findAllNumPE();
    
    @Query("FROM Utilisateur WHERE numRCS IS NOT NULL ORDER BY idutilisateur DESC")
    List<Utilisateur> findAllNumRCS();
    
    @Query("FROM Utilisateur WHERE numPE IS NOT NULL AND formation = ?1")
    List<Utilisateur> findByFormation(String formation);
    
    @Query("FROM Utilisateur WHERE projetSoutenu = ?1")
    List<Utilisateur> findAllProjetSoutenu(Long projetSoutenu);
    
    @Query("FROM Utilisateur WHERE email=?1 AND password=?2")
    Utilisateur login(String email, String password);
    
}
