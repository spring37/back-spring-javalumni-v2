package com.astonprojet.javalumni.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.astonprojet.javalumni.models.ReponseFaq;

@Repository
public interface ReponseFaqRepository extends JpaRepository<ReponseFaq, Long>{


}